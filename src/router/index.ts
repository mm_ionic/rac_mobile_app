import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import HomePage from '@/views/HomePage.vue'
import MyProfile from '@/views/my-profile/my-profile.vue'
import MyChat from '@/views/my-chat/my-chat.vue'
import MyGames from '@/views/my-games/my-games.vue'
import GameDetail from '@/views/game-detail/game-detail.vue'
import MyLeague from '@/views/my-leagues/my-leagues.vue'
import LeagueDetail from '@/views/league-detail/league-detail.vue'
import MyTeam from '@/views/my-teams/my-teams.vue'
import TeamDetail from '@/views/team-detail/team-detail.vue'
import TeamPerson from '@/views/team-person/team-person.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    // redirect: '/tabs/tab1'
    name: 'LoginPage',
    component: () => import('@/views/user-login/user-login.vue')
  },
  {
    path: '/home/',
    name: "HomePage",
    component: HomePage,
    children: [
      {
        path: '',
        redirect: '/home/my-games'
      },
      {
        name: "MyProfile",
        path: 'my-profile',
        component: MyProfile
      },
      {
        path: 'chat',
        name: 'ChatPage',
        component: MyChat
      },
      {
        path: 'my-games',
        name: 'MyGames',
        component: MyGames
      },

      {
        name: 'GameDetail',
        path: 'game/:id',
        component: GameDetail
      },
      {
        path: 'league/:id',
        name: 'LeagueDetail',
        component: LeagueDetail
      },
      {
        path: 'leagues',
        name: 'LeaguesPage',
        component: MyLeague
      },
      {
        path: 'my-teams',
        name: 'MyteamPage',
        component: MyTeam
      },
      {
        path: 'team/:id',
        name: 'TeamDetail',
        component: TeamDetail
      },
      {
        path: 'person/:id',
        name: 'PersonDetail',
        component: TeamPerson
      },
    ]
  },
  {
    path: '/test/',
    component: HomePage
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
