import { defineComponent, ref } from "vue";
import { 
  IonPage, 
  IonContent, 
  IonHeader, 
  IonToolbar, 
  IonRow, 
  IonButton, 
  IonCol, 
  IonGrid, 
  IonTitle, 
  IonImg,
  IonCard,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  alertController,
  IonCardHeader
 } from '@ionic/vue';
import ExploreContainer from '@/components/ExploreContainer.vue';

export default defineComponent({
  name: "MyProfile",
  components: { ExploreContainer, IonHeader, IonToolbar, IonPage, IonContent, IonRow, IonButton, IonCol, IonGrid, IonTitle, IonCard, IonCardSubtitle, IonCardTitle, IonCardContent, IonImg, IonCardHeader},
  setup() {
    const content = ref();
    return { content };
  },

  mounted() {
    this.loadUser()
  },

  data() {
    return {
      editMode: false,
      form: {
        id: null,
        name: null,
        gender: null,
        email: null,
        phone_number: null,
        instagram: null,
        facebook: null,
        twitter: null,
        tiktok: null,
      }
    }
  },

  computed: {
    profile():object | null {
      return this.$store.getters['user/GET_USER']
    }
  },

  watch: {
    profile: function() {
      this.form = this.$Helper.clone(this.profile)
    }
  },
  methods: {
    toggleEdit() {
      this.editMode = true
    },
    exitEditMode() {
      this.editMode = false
      console.log(this.editMode, 'editmode')
    },
    async updateUser() {
      this.$store.dispatch('ui/SHOW_LOADING', 'loading')
      try {
        const data = {
          user: {
            id: this.form.id,
            name: this.form.name,
            phone_number: this.form.phone_number,
            email: this.form.email,
            gender: this.form.gender,
            instagram: this.form.instagram,
            tiktok: this.form.tiktok,
            twitter: this.form.twitter,
            facebook: this.form.facebook,

          }
        }
        await this.$ApiPlugin.user.update(data)

        this.loadUser()

        this.editMode = false

        this.$store.dispatch('ui/SHOW_ALERT', {header: 'User updated!'})
        
      } catch (err) {
        this.$store.dispatch('ui/SHOW_ALERT', {header: 'Ops! something went wrong'})
      } finally {
        this.$store.dispatch('ui/HIDE_LOADING', null, {root: true})
      }
    },
    async loadUser() {
      try {
        const response = await this.$ApiPlugin.user.me()
        this.$store.commit("user/SET_USER", response.data)
      } catch (err) {
        console.log(err)
      }
    },
    async logout() {
      await this.$MyStorage.storage.clear('token')
      this.$router.push('/')
    }
  }
  
});