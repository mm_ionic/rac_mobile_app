
import { defineComponent } from 'vue';
import { IonTabBar, IonTabButton, IonTabs, IonLabel, IonIcon, IonPage, IonRouterOutlet } from '@ionic/vue';
import { peopleOutline, personOutline, caretForwardCircleOutline, listCircleOutline, chatbubbleOutline } from 'ionicons/icons';

export default defineComponent({
  name: 'HomePage',
  components: { IonLabel, IonTabs, IonTabBar, IonTabButton, IonIcon, IonPage, IonRouterOutlet },
  setup() {
    return {
      peopleOutline, personOutline, caretForwardCircleOutline, listCircleOutline, chatbubbleOutline
    }
  },

  mounted() {
    this.loadUser()
  },

  data() {
    return {
      loaded: false
    }
  },

  methods: {

    async loadUser() {
      try {
        const response = await this.$ApiPlugin.user.me()
        this.$store.commit("user/SET_USER", response.data)
        this.$router.push('/home/') 
        this.loaded = true
      } catch (err) {
        this.$router.push('/')
        console.log(err)
      }
    },
  }
});
