import { defineComponent, ref } from "vue";
import { IonPage, loadingController, alertController, IonInput, IonItem, IonContent, IonRow, IonButton, IonCol, IonGrid, IonIcon } from '@ionic/vue';
import { useRouter } from 'vue-router';
import { arrowForward } from 'ionicons/icons';


export default defineComponent({
  name: "UserLogin",
  components: { IonPage, IonInput, IonContent, IonItem, IonRow, IonButton, IonCol, IonGrid, IonIcon },
  data: function () {
    return {
      toggleView: 'welcome',
      form: {
        email: '',
        // password: 'nTFZO5dPQ2dXu2mJ2OkZ'
        password: ''
      },

      token: null,
      arrowForward: arrowForward,
      router: useRouter()
    }
  },
  setup() {
    const content = ref();
    return { content };
  },

  mounted() {
    // console.log(this.$store.getters.userToken, 'userToken');
    this.checkMe()
  },

  methods: {

    async DoLogin (): Promise<void> {

      // loading.present();
      // this.$Helper.showLoading('Authorizing');
      this.$store.dispatch('ui/SHOW_LOADING', 'Authorizing');

      this.$ApiPlugin.user.login(this.form).then(async (response: any) => {
        /**
         * set to local storage
         */
         const token = response.data.access_token

         await this.$MyStorage.storage.set('token', token)
 
         this.$store.commit('user/SET_TOKEN', token);

        // GET user object and store it on VueX
          const user = await this.$ApiPlugin.user.me()
          this.$store.commit("user/SET_USER", user.data)

        //  this.$Helper.showAlert('Login Success!', 'Redirecting');
         this.$store.dispatch('ui/SHOW_ALERT', {header: 'Success', message: ''});

        this.router.push('/home/')

        // this.checkMe()

      }).catch(async (err: any) => {

        this.$store.dispatch('ui/SHOW_ALERT', {header: 'Failed', message: 'Wrong username/password combination'});

      }).finally( () => {
        
        // this.$Helper.hideLoading();
        this.$store.dispatch('ui/HIDE_LOADING');
        
      });

    },

    checkMe(): void {
      this.$store.dispatch('ui/SHOW_LOADING', 'Loading');
      this.$ApiPlugin.user.me().then(() => {
        this.router.push('/home/')
      }).catch().finally(() => {
        this.$store.dispatch('ui/HIDE_LOADING');
      });
    }
  }
});