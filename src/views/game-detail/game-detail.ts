import { defineComponent, ref } from 'vue';
import { 
  IonPage, 
  IonHeader, 
  IonToolbar, 
  IonTitle, 
  IonContent,
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonImg,
  IonButton,
} from '@ionic/vue';
import ExploreContainer from '@/components/ExploreContainer.vue';
import AppHeader from '@/components/AppHeader/AppHeader.vue'
import { caretForwardOutline } from 'ionicons/icons'
import StatTable from '@/components/StatTable/StatTable.vue'
import PlaybyPlay from '@/components/PlaybyPlay/PlaybyPlay.vue'
import positionNames from '@/mixins/positionNames'
import { statTitles } from '@/mixins/statTitles'

export default defineComponent({
  name: 'MyLeagues',
  mixins: [positionNames, statTitles],

  components: { AppHeader, ExploreContainer, IonHeader, IonToolbar, IonTitle, IonContent, IonPage,
    IonCard,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonImg,
    IonButton,
    IonCardContent,
    StatTable,
    PlaybyPlay
  },
  setup() {
    const content = ref(0);

    const activeSegment = ref('boxscore');

    const boxScoreView = ref('home');

    return { content, activeSegment, caretForwardOutline, boxScoreView };
  },

  data() {
    return {
      game: false,
    }
  },

  mounted() {
    this.loadData()

    console.log(this.passing_stats_main(), 'passing_stats_main')
    return 
  },


  computed: {
    game_id() {
      return this.$route.params.id
    }
  },

  methods: {

    async loadData() {

      this.$store.dispatch('ui/SHOW_LOADING', 'Loading..')
      
      try {
        const response = await this.$ApiPlugin.games.detail(this.game_id)
        this.game = response.data.payload.game
        // this.$store.commit("SET_USER", response.data)
      } catch (err) {
        console.log(err)
      } finally {
        this.$store.dispatch('ui/HIDE_LOADING')
      }
    }
  }
});
