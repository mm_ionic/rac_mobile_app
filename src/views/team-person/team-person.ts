import { defineComponent, ref } from "vue";
import { IonSegmentButton, alertController, IonSlide, IonSlides, IonAvatar, IonLabel, IonList, IonItem, IonPage, IonContent, IonRow, IonButton, IonIcon, IonCol, IonGrid, IonHeader, IonToolbar, IonTitle, IonRouterOutlet, IonSegment } from '@ionic/vue';
import LongTable from '@/components/LongTable/LongTable.vue'
import StatTable from '@/components/StatTable/StatTable.vue'
import AppHeader from '@/components/AppHeader/AppHeader.vue'
import { arrowForward } from 'ionicons/icons'
import { statTitles } from '@/mixins/statTitles'
import { logoFacebook, logoTwitter, logoTiktok, logoInstagram } from 'ionicons/icons';


// REF: https://racweb.local/player-profile/brianne-armstrong-23
export default defineComponent({
  name: "TeamPerson",
  components: {
    AppHeader,
    IonPage,
    IonContent,
    IonRow,
    IonButton,
    IonCol, 
    IonGrid, 
    LongTable, 
    StatTable,
    IonSlide,
    IonSlides,
    IonHeader, 
    IonToolbar, 
    IonTitle, 
    IonRouterOutlet, 
    IonSegment, 
    IonIcon,
    IonAvatar,
    IonItem,
    IonSegmentButton,
    IonLabel,
    IonList
  },

  data () {
    return {
      playerData: {},
      loaded: false,
      fields: [
        {
          key: 'last_name',
          label: 'Last Name'
        },
        {
          key: 'first_name',
          label: 'First Name'
        },
        {
          key: 'age',
          label: 'Person age',
        },
        {
          key: 'last_name',
          label: 'Last Name'
        },
        {
          key: 'first_name',
          label: 'First Name'
        },
        {
          key: 'age',
          label: 'Person age',
        }
      ],
      items: [
        { isActive: true, age: 40, first_name: 'Dickerson', last_name: 'Macdonald' },
        { isActive: false, age: 21, first_name: 'Larsen', last_name: 'Shaw' },
        { isActive: false, age: 89, first_name: 'Geneva', last_name: 'Wilson' },
        { isActive: true, age: 38, first_name: 'Jami', last_name: 'Carney' }
      ]
    }
  },

  watch: {
    activeSegment: function() {
      // this.$refs.slides.$el.slideTo(parseInt(this.activeSegment))
    },

    player_id: function () {
      this.loadData()
    }
  },

  setup() {
    const content = ref(0);
    const activeSegment = ref('0');
    const slides = ref(null);

    return { content, activeSegment, arrowForward, slides, logoFacebook, logoTwitter, logoTiktok, logoInstagram };
  },

  mounted() {
    this.loadData()
  },

  computed: {
    player_id() {
      return this.$route.params.id
    }
  },

  mixins: [statTitles],

  methods: {


    openTeamDetail(team: any) {
      this.$store.commit('app/SET_TEAM', team);
      this.$router.push(`/home/team/${team.id}`)
    },

    async loadData() {
      this.$store.dispatch('ui/SHOW_LOADING', 'loading')
      try {
        const response = await this.$ApiPlugin.user.player({id: this.player_id})
        this.playerData = response.data.payload
        this.loaded = true
      } catch (err) {
        console.log(err)
      } finally {
        this.$store.dispatch('ui/HIDE_LOADING')
      }
    },

    onSlideChange(event: any) {
      event.target.getActiveIndex().then(
        (index: any)=>{
          console.log(index)
          this.activeSegment = index
        });
    },
  }

});
