import { defineComponent, ref } from "vue";
import { IonPage, IonContent, IonRow, IonButton, IonCol, IonGrid } from '@ionic/vue';
import { useRouter } from 'vue-router';
import Echo from 'laravel-echo';

export default defineComponent({
  name: "MyChat",
  components: { IonPage, IonContent, IonRow, IonButton, IonCol, IonGrid },
  data() {
    return {
      messages: new Array<any>([]),
    }
  },
  setup() {
    const content = ref([]);
    const router = useRouter();
    const DoLogin = () => {
      router.push('/tabs/tab1')
    };

    
    return { content, DoLogin };
  },

  mounted() {

    const echo = new Echo({
        broadcaster: 'pusher',
        key: process.env.VUE_APP_PUSHER_APP_KEY,
        cluster: process.env.VUE_APP_PUSHER_APP_CLUSTER,
        encrypted: false,
        wsHost: process.env.VUE_APP_WEBSOCKET_HOST,
        wsPort: process.env.VUE_APP_WEBSOCKET_PORT,
        disableStats: true,
        enabledTransports: ['ws']
    });
    echo.channel('chat').listen('.pesanbaru', (e:any) => {
      console.log(e, 'event')
      this.messages.push({
          text: e.message,
          user: e.user
      });
    });

    console.log('here!')

  },
});

