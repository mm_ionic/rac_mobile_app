import { defineComponent, ref } from "vue"
import { alertController } from "@ionic/vue"
import LongTable from '@/components/LongTable/LongTable.vue'
import GamesTable from '@/components/GamesTable/GamesTable.vue'
import AppHeader from '@/components/AppHeader/AppHeader.vue'
import { arrowForward } from 'ionicons/icons'
import positionNames from '@/mixins/positionNames'
import StatTable from '@/components/StatTable/StatTable.vue'
import { statTitles } from "@/mixins/statTitles"


export default defineComponent({
  name: "TeamDetail",
  components: {
    AppHeader,
    LongTable, 
    GamesTable,
    StatTable
  },
  mixins: [positionNames, statTitles],

  data () {
    return {
      fields: [
        {
          key: 'name',
          label: "Name"
        },
        {
          key: 'team',
        },
        {
          key: 'rate',
          label: 'Yards'
        },
        {
          key:  'yrd/rec',
          label: 'Yards'
        },
        {
          key: 'comps',
          label: 'Yards'
        },
        {
          key: 'drop_%',
          label: '%',
        }
      ],
      items: [
        { isActive: true, age: 40, first_name: 'Dickerson', last_name: 'Macdonald' },
        { isActive: false, age: 21, first_name: 'Larsen', last_name: 'Shaw' },
        { isActive: false, age: 89, first_name: 'Geneva', last_name: 'Wilson' },
        { isActive: true, age: 38, first_name: 'Jami', last_name: 'Carney' }
      ],
      scheduleFields: [
        'team_name',
        'score',
        'schedule'
      ],
      roster: [],
      stats: [],
      schedules: null,
      scheduleItems: []
    }
  },

  setup() {
    const content = ref(0);

    const activeSegment = ref('roster');

    return { content, activeSegment, arrowForward };
  },

  mounted() {
    this.loadData()
  },

  computed: {
    teamDetail() {
      return this.$store.getters['app/GET_TEAM'];
    }
  },

  methods: {
    async loadData() {
      this.$store.dispatch('ui/SHOW_LOADING', 'loading')
      try {
        // const response = await this.$ApiPlugin.team.team_games({
        //   team_id: this.$route.params.id
        // })
        // this.schedules = response.data.payload.games

        const roster = await this.$ApiPlugin.team.roster(this.$route.params.id)
        this.roster = roster.data.payload

        const params = {
          playerType: 'wr',
          league_id: 1,
          team_id: this.$route.params.id
        }
        const stats = await this.$ApiPlugin.team.fetchStats(params)
        this.stats = stats.data.payload

        // const response2 = await this.$ApiPlugin.team.teamGame({
        //   team_id: this.$route.params.id,
        //   per_page: 10,
        //   current_page: 1
        // })
        // this.scheduleItems = response2.data.payload.games

      } catch (err: any) {
        const alert = await alertController.create({
          header: 'Failed!',
          message: err?.message,
          buttons: ['OK']
        });
  
        await alert.present();
      } finally {
        this.$store.dispatch('ui/HIDE_LOADING')
      }
    }
  }

});