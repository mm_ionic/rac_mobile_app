import { defineComponent, ref } from "vue";
import { IonList, IonItem, IonPage, IonLabel, IonAvatar, IonContent, IonRow, IonButton, IonIcon, IonCol, IonGrid, IonHeader, IonToolbar, IonTitle, IonRouterOutlet, IonSegment } from '@ionic/vue';
import LongTable from '@/components/LongTable/LongTable.vue'
import AppHeader from '@/components/AppHeader/AppHeader.vue'
import {arrowForward } from 'ionicons/icons'

export default defineComponent({
  name: "MyTeams",
  components: { AppHeader, IonItem, IonLabel, IonAvatar, IonList, IonPage, IonContent, IonRow, IonButton, IonCol, IonGrid, LongTable, IonHeader, IonToolbar, IonTitle, IonRouterOutlet, IonSegment, IonIcon },

  setup() {
    const content = ref(0);

    const activeSegment = ref('roster');

    return { content, activeSegment, arrowForward };
  },

  data() {
    return {
      teams: []
    }
  },

  mounted() {
    console.log('this')
    this.loadData()
  },

  methods: {
    segmentChanged(ev: CustomEvent) {
      // console.log('Segment changed', ev);
    },
    openTeamDetail(record: any) {
      this.$store.commit('app/SET_TEAM', record);
      this.$router.push(`/home/team/${record.team.id}`)
    },
    async loadData() {
      this.$store.dispatch('ui/SHOW_LOADING', 'loading')
      try {
        const response = await this.$ApiPlugin.team.my_team(this.$store.getters['user/GET_USER'].id)
        this.teams = response.data.payload
      } catch (err) {
        console.log(err)
      } finally {
        this.$store.dispatch('ui/HIDE_LOADING')
      }
    }
  },
});