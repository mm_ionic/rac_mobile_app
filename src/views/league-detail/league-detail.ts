import { defineComponent, ref } from 'vue';
import { 
  IonPage, 
  IonHeader, 
  IonToolbar, 
  IonTitle, 
  IonContent,
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonImg,
  IonButton,
} from '@ionic/vue';
import ExploreContainer from '@/components/ExploreContainer.vue'
import AppHeader from '@/components/AppHeader/AppHeader.vue'
import GamesTable from '@/components/GamesTable/GamesTable.vue'
import LongTable from '@/components/LongTable/LongTable.vue'
import StatTable from '@/components/StatTable/StatTable.vue'
import positionNames from '@/mixins/positionNames'
import { statTitles } from '@/mixins/statTitles';

export default defineComponent({
  name: 'LeagueDetail',
  components: { AppHeader, ExploreContainer, IonHeader, IonToolbar, IonTitle, IonContent, IonPage,
    IonCard,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonImg,
    IonButton,
    IonCardContent,
    GamesTable,
    LongTable,
    StatTable
  },
  mixins: [positionNames, statTitles],
  setup() {
    const content = ref(0);

    const activeSegment = ref('standing');

    return { content, activeSegment };
  },

  data() {
    return {
      games: [],
      leagues: [],
      fields: [
        {
          key: 'name',
          label: "Name"
        },
        {
          key: 'team',
        },
        {
          key: 'rate',
          label: 'Yards'
        },
        {
          key:  'yrd/rec',
          label: 'Yards'
        },
        {
          key: 'comps',
          label: 'Yards'
        },
        {
          key: 'drop_%',
          label: '%',
        }
      ],
      myParams: {},
      items: [
        { isActive: true, age: 40, first_name: 'Dickerson', last_name: 'Macdonald' },
        { isActive: false, age: 21, first_name: 'Larsen', last_name: 'Shaw' },
        { isActive: false, age: 89, first_name: 'Geneva', last_name: 'Wilson' },
        { isActive: true, age: 38, first_name: 'Jami', last_name: 'Carney' }
      ],
      stats: {
        psr:{},
        qb: {},
        wr: {},
        d: {},
        r: {}
      }
    }
  },

  mounted() {
    this.loadData()

    this.myParams = {
      league_id: this.leagueDetail.id,
      formatType: 'league_total_player',
      playerType: 'd',
    }
    return 
  },

  watch: {
    '$route.params.id': function(){
      this.loadData()
    }
  },

  computed: {
    leagueDetail(this: any) {
      return this.$store.getters['app/GET_LEAGUE'];
    }
  },

  methods: {
    async loadData() {
      return
    }
  }
});
