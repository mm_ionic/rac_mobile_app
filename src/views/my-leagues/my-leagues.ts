import { defineComponent, ref } from 'vue';
import { 
  IonPage, 
  IonHeader, 
  IonToolbar, 
  IonTitle, 
  IonContent,
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonImg,
  IonButton,
} from '@ionic/vue';
import ExploreContainer from '@/components/ExploreContainer.vue';
import AppHeader from '@/components/AppHeader/AppHeader.vue'

export default defineComponent({
  name: 'MyLeagues',
  components: { AppHeader, ExploreContainer, IonHeader, IonToolbar, IonTitle, IonContent, IonPage,
    IonCard,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonImg,
    IonButton,
    IonCardContent 
  },
  setup() {
    const content = ref(0);

    const activeSegment = ref('roster');

    return { content, activeSegment };
  },

  data() {
    return {
      leagues: [],
    }
  },

  mounted() {
    this.loadData()
    return 
  },

  methods: {

    setLeague (record: any) {
      this.$store.commit('app/SET_LEAGUE', record);
      this.$router.push(`/home/league/${record.id}`)
    },
    async loadData() {
      this.$store.dispatch('ui/SHOW_LOADING', 'loading')
      try {
        const response = await this.$ApiPlugin.league.index({
          table: {
            per_page: 1,
            sort_desc: 'dec',
            sort_by: 'created_at'
          }
        })
        this.leagues = response.data.payload.leagues
        // this.$store.commit("SET_USER", response.data)
      } catch (err) {
        console.log(err)
      } finally {
        this.$store.dispatch('ui/HIDE_LOADING')
      }
    }
  }
});
