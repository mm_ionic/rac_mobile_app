import { defineComponent, ref } from "vue";
import LongTable from '@/components/LongTable/LongTable.vue'
import GamesTable from '@/components/GamesTable/GamesTable.vue'
import AppHeader from '@/components/AppHeader/AppHeader.vue'
import * as moment from 'moment';

export default defineComponent({
  name: "MyGames",
  components: {
    LongTable,
    GamesTable,
    AppHeader,
    moment
  },
  setup() {
    const content = ref();


    return { content };
  },

  data() {
    return {
      upcomings: [],
      recents: [],
      activeSegment: 'upcoming'
    }
  },
  mounted (){
    this.loadData()
  },

  computed: {
    profile():any {
      return this.$store.getters['user/GET_USER']
    }
  },

  methods: {


    async loadData() {
      this.$store.dispatch('ui/SHOW_LOADING', 'loading')
      try {
        const response = await this.$ApiPlugin.games.my_upcoming_games({
          user_id: this.profile.id,
          per_page: 10,
          current_page: 1
        })
        const response2 = await this.$ApiPlugin.games.my_recent_games({
          user_id: this.profile.id,
          per_page: 10,
          current_page: 1
        })
        this.upcomings = response.data.payload.games
        this.recents = response2.data.payload.games
      } catch (err) {
        console.log(err)
      } finally {
        this.$store.dispatch('ui/HIDE_LOADING', 'loading')
      }
    }
  }
});