/* eslint-disable */

declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

import { Store } from "vuex";
import { State } from "./store";
import {apiCollection} from './api/api';
import {apiCollection} from './api/api';
import {helperCollection} from './plugins/helper';
import {myStorageConst} from './plugins/ionStorage';


const storage = new Storage()

storage.create()

declare module "@vue/runtime-core" {
  interface ComponentCustomProperties {
    $store: Store<any>;
    $ApiPlugin: apiCollection;
    $Helper: helperCollection;
    $MyStorage: myStorageConst;
  }
}