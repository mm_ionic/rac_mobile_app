import { defineComponent, ref } from "vue";
import { IonRow, IonCol, IonIcon } from '@ionic/vue';
import { notificationsCircle } from 'ionicons/icons';

export default defineComponent({
    name: "ScheduleTable",
    components: {
        IonRow,
        IonCol,
        IonIcon
    },
    props: {
        fields: {
            type: [Object, Array],
            default: function () {
                return {}
            }
        },
        items: {
            type: Array,
            default: function () {
                return []
            }
        },
    },
    setup() {
        const content = ref();

        return { content, notificationsCircle };
    },
    methods: {
        getFieldKey(field: any): string {
            return Object.prototype.hasOwnProperty.call(field, 'key') ? field.key : field.toString()
        }
    }
});