import { defineComponent, ref } from "vue";
import { IonBackButton, IonButtons, IonHeader, IonToolbar, IonGrid, IonRow, IonCol, IonTitle } from '@ionic/vue';

export default defineComponent({
    name: "AppHeader",
    components: { IonBackButton, IonButtons, IonHeader, IonToolbar, IonGrid, IonRow, IonCol, IonTitle },
    props: {
        desktop: {
            type: Boolean,
            default: false
        },
        pageTitle: {
            type: String,
            default: ''
        },
        subPage: {
            type: Boolean,
            default: false
        }
    },

    setup() {
        const content = ref();

        return { content };
    }
});