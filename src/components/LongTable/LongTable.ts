import { defineComponent, ref } from "vue";
import { arrowBackCircle, arrowForwardCircle } from 'ionicons/icons';

export default defineComponent({
    name: "LongTable",

    props: {
        fields: {
            type: [Object, Array],
            default: function () {
                return {}
            }
        },
        items: {
            default: function () {
                return []
            }
        },
        freezeTop: {
            type: Boolean,
            default: true
        },
        freezeLeft: {
            type: Boolean,
            default: true
        },
        perPage: {
            default: 5
        }
    },


    data() {
        return {
            curPage: 1,
            ascending: false,
            sortColumn: '',
        }
    },

    setup() {
        const content = ref()
        return { content, arrowBackCircle,  arrowForwardCircle };
    },

    mounted(this: any) {
        this.data = this.items
    },
    computed: {
        // why using this here?
        // super thanks to SO thread: https://stackoverflow.com/questions/66055201/vue3-cannot-access-computed-prop-from-another-computed-prop-when-using-typescri
      totalPages(this: any): number {
        return Math.ceil(this.items.length / this.perPage)
      },
      parsedItems(this: any): Array<any> {
        if (this.data) {
            const data = this.items
            const col = this.sortColumn.key
            const ascending = this.ascending

            const result =  data.sort(function(a: any, b: any) {
                if (a[col] > b[col]) {
                    return ascending ? 1 : -1
                } else if (a[col] < b[col]) {
                    return ascending ? -1 : 1
                }
                return 0;
            }).slice((this.curPage-1) * this.perPage, this.curPage * this.perPage)

            return result
        } 
        return this.data
      }
    },
    methods: {
        sortTable(col: any) {
            if (this.sortColumn === col) {
                this.ascending = !this.ascending;
            } else {
                this.ascending = true;
                this.sortColumn = col;
            }
        },
        formatDefaultLabel (val: string) {
            if (val) {
                const newval = val.replace(/_/g, ' ').replace(/-/g, ' ')
                return newval.charAt(0).toUpperCase() + newval.slice(1)
            }
            return val
        },
        prev(): void {
            if (this.curPage > 1) {
                this.curPage = this.curPage-1
            }
        },
        next(): void {
            if (this.curPage < this.totalPages) {
                this.curPage = this.curPage+1
            }
        },
        getFieldKey(field: any): string {
            return Object.prototype.hasOwnProperty.call(field, 'key') ? field.key : field.toString()
        }
    }
});