import { defineComponent, ref } from "vue"
import LongTable from '@/components/LongTable/LongTable.vue'
import { statTitles } from '@/mixins/statTitles'
import positionNames from '@/mixins/positionNames'


export default defineComponent({
    name: "StatTable",
    components: {
        LongTable
    },
    mixins: [statTitles, positionNames],
    props: {
        params: {
            default: null
        },
        items: {
            default: []
        },
        bannedField: {
            default: []
        },
        hasName: {
            default: true
        },
        onlyFields: {
            default: []
        }
    },

    data() {
        return {
            records: new Array<any>(),
            fields:  new Array<any>(),
        }
    },
    setup() {
        const content = ref();

        return { content };
    },

    mounted: function() {
        if (this.hasName) {
            this.fields.push({
                key: 'name',
                label: "Name"
            })
        }
        if (this.params == null) {
            if (Array.isArray(this.items)) {
                this.records = JSON.parse(JSON.stringify(this.items))
            } else {
                this.records.push(this.items)
            }
            this.generateFieldLabel()
        } else {
            this.loadData()
        }
    },

    computed: {
        bannedProperties(): Array<string> {
            return [
                'user_id',
                'slug',
                'avatar',
                'name',
                'first',
                'last',
                'team_id',
                'created_at',
                'created_by',
                'deleted_at',
                'updated_at',
                'league'
            ]
        }
    },

    methods: {
        mySlot(field: any) {
            return field.key
        },
        generateFieldLabel() {
            if (this.onlyFields && this.onlyFields.length > 0) {
                Object.keys(this.onlyFields).forEach((val, index) => {
                    if (typeof this.onlyFields[index] === 'object'  && !Array.isArray(val) && val !== null) {
                        const key = this.onlyFields[index]['key']
                        const obj = {
                            label: this.onlyFields[index]['label'],
                            // label: val,
                            key: key,
                        }
                        this.fields.push(obj)
                    }else {
                        const key = this.onlyFields[index]
                        const obj = {
                            label: this.description_abb(key),
                            // label: val,
                            key: key,
                        }
                        this.fields.push(obj)
                    }
                })
            } else {
                Object.keys(this.records[0]).forEach((val, index) => {
                // banned 
                // console.log(statTitles.methods, 'ets')
                    if (!this.bannedProperties.includes(val)) {
                        const obj = {
                            label: this.description_abb(val),
                            // label: val,
                            key: val
                        }
                        this.fields.push(obj)
                    }
                })
            }
        },
        async loadData() {
            try {
                const response4 = await this.$ApiPlugin.team.fetchStats(this.params)
    
                this.records = response4.data.payload
                this.generateFieldLabel()
                
            } catch (err) {
                console.log(err)
            }
        },
        getFieldKey(field: any): string {
            return Object.prototype.hasOwnProperty.call(field, 'key') ? field.key : field.toString()
        },

        test() {
            console.log(this.description_abb('test'))
        }
    }
});