import { defineComponent, ref } from "vue"
import { IonRow, IonCol, IonIcon } from '@ionic/vue'
import { notificationsCircle } from 'ionicons/icons'
import moment from 'moment'

import { arrowBackCircle, arrowForwardCircle } from 'ionicons/icons';

export default defineComponent({
    name: "GamesTable",
    components: {
        IonRow,
        IonCol,
        IonIcon
    },
    props: {
        params: {
            default: function () {
                return {}
            }
        },
        // games: {
        //     type: Object,
        //     default: function () {
        //         return {}
        //     }
        // },
    },
    data() {
        return {
            games: [],
            curPage: 1
        }
    },
    setup() {
        const content = ref();

        return { content, notificationsCircle, arrowBackCircle, arrowForwardCircle };
    },
    async mounted(this: any) {
        this.loadData()
    },
    
    methods: {
        async loadData(this: any) {
            try {
                let response = null
                if (this.params.type == 'upcoming') {
                    response = await this.$ApiPlugin.games.my_upcoming_games({
                        user_id: this.params.user_id,
                        per_page: this.params.per_page,
                        page: this.curPage
                      })
                } else if (this.params.type == 'recent') {
                    response = await this.$ApiPlugin.games.my_recent_games({
                        user_id: this.params.user_id,
                        per_page: this.params.per_page,
                        page: this.curPage
                      })
                } else if (this.params.type == 'team_games') {
                    response = await this.$ApiPlugin.team.teamGame({
                        team_id: this.params.team_id,
                        per_page: this.params.per_page,
                        page: this.curPage
                      })
                }

                this.games = response.data.payload.games
              } catch (err) {
                console.log(err)
              } 
        },
        async showPrev() {
            this.curPage--
            await this.loadData()
        },
        async showMore() {
            this.curPage++
            await this.loadData()
        },
        moment() {
            return moment()
        },

        formatDate(date: string, format: string) {
            const date1 = new Date(date)
            
            return moment(date1).format(format)

        },
        getFieldKey(field: any): string {
            return Object.prototype.hasOwnProperty.call(field, 'key') ? field.key : field.toString()
        },
        
        setGame (record: any) {
            // this.$store.commit(MutationTypes.SET_GAME, record);
            this.$router.push(`/home/game/${record.id}`)
        },
    }
});