import { defineComponent, ref } from "vue"
import { IonRow, IonCol, IonIcon } from '@ionic/vue'
import { notificationsCircle } from 'ionicons/icons'
import moment from 'moment'


export default defineComponent({
    name: "GamesTable",
    components: {
        IonRow,
        IonCol,
        IonIcon
    },
    props: {
        games: {
            type: Object,
            default: function () {
                return {}
            }
        },
    },
    setup() {
        const content = ref();

        return { content, notificationsCircle };
    },
    methods: {
        moment() {
            return moment()
        },

        formatDate(date: string, format: string) {
            const date1 = new Date(date)
            
            return moment(date1).format(format)

        },
        getFieldKey(field: any): string {
            return Object.prototype.hasOwnProperty.call(field, 'key') ? field.key : field.toString()
        },
        
        setGame (record: any) {
            // this.$store.commit(MutationTypes.SET_GAME, record);
            this.$router.push(`/home/game/${record.id}`)
        },
    }
});