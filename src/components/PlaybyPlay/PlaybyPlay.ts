import { defineComponent, ref } from "vue";

export default defineComponent({
  name: "ScheduleTable",
  components: {},
  props: {
    game: {
      type: Object,
      default: () => { return {} }
    }
  },

  computed: {
    drives: function () {
      if (this.game) {
        const arr:any = []
        this.game.plays.filter((v: any) => {
          if (!arr.includes(v.drive)) {
            arr.push(v.drive)
          }
        })
        return arr
      }
      return []
    }
  },
  methods: {
    resultOfDrive(index: any,) {
      const arr:any = []
      this.game.plays.filter((v: any) => {
        console.log(v.drive)
        if (v.drive === index) {
          arr.push(v)
        }
      })
      const last_play = arr[arr.length - 1];
      let team = '';
      if(this.$props.game.away_team_id === arr[0].o_team_id){
        team = this.$props.game.away_team.team_media.name
      }else{
        team = this.$props.game.home_team.team_media.name
      }
      let i;
      let td = 0;
      let xp = 0;
      let int = 0;
      for(i = 0 ; i < arr.length ; i++){
          td = td + parseInt(arr[i].td)
          int = int + parseInt(arr[i].int)
          xp = xp + parseInt(arr[i].xp)

      }
      console.log('TDs count')
      console.log(td)
      if(td >= 1){
          if (xp === 1) {
              return team+': touchdown xp 1';
          }
          if (xp >= 2) {
              return team+': touchdown xp 2';
          }
          if (int >= 1) {
            return team+': Pick Six';
        }
          return team+': touchdown';

      }
      if (int >= 1) {
          return team+': interception';
      }
      if (last_play.down == 4) {
          return team+': No Score';
      }
      if (last_play.down == 3) {
          return team+': No Score';
      }
      return team+': No Score';
  },
    playMessage (data: any) {
      const qb_name = data.qb.first_name + ' ' + data.qb.last_name
      const wr_name = data.wr.first_name + ' ' + data.wr.last_name
      const d_name = data.d.first_name + ' ' + data.d.last_name

      let sentence = '';
      const play = data;
      let td = '';
      let first_down = '';
      let xp = '';
      let safety = '';
      let drop = '';
      let pd = '';
      if (play.td == 1) {
        td = 'for a TD '
      }
      if (play.first_down == 1) {
        first_down = 'for a First '
      }
      if (play.xp > 0) {
        xp = 'for a ' + play.xp + ' XP'
      }
      if (play.safety == 1) {
        safety = 'for a safety'
      }
      if (play.pass_drop == 1) {
        drop = 'dropped by ' + wr_name
      }
      if (play.pass_pd == 1) {
        pd = 'pass deflected by  ' + d_name
      }
      if (play.comp == 1) {
        sentence = qb_name + ' to ' + wr_name + ' for ' + play.yards + ' yards ' + td + xp + first_down;
      }
      if (play.int == 1) {
        sentence = qb_name + ' Intercepted by ' + d_name + ' for ' + play.yards + ' yards ' + td + xp + first_down;
      }
      if (play.sack == 1) {
        sentence = qb_name + ' Sacked by ' + d_name + " " + safety;
      }
      if (play.incomp == 1) {
        sentence = qb_name + ' threw an Incomplete Pass  ' + pd + drop;
      }
      return sentence;
      // this.message(sentence, 'Play Added');
    },
  }
})