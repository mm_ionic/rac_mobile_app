import { createApp } from 'vue'
import App from './App.vue'
import router from './router';
// import Plugins from './plugins/helpers';

import { IonicVue } from '@ionic/vue';

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

/* Theme variables */
import './theme/variables.css';
import './theme/styles.scss';

// import helpers from './plugins/helpers';
import {store } from './store'; 
import {ApiPlugin} from './api/api';
import {HelperPlugin} from './plugins/helper';

import {MyStoragePLugin} from './plugins/ionStorage';

const app = createApp(App)
  .use(IonicVue)
  .use(store)
  .use(ApiPlugin)
  .use(HelperPlugin)
  .use(MyStoragePLugin)
  .use(router)
  ;
  

import component_registration from './plugins/component_registration'

(new component_registration(app)).init();

window.Pusher = require('pusher-js');

router.isReady().then(() => {
  app.mount('#app');
});