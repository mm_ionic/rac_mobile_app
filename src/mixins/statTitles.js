export const statTitles =  {
    methods: {
        description (stat) {

            let type;
            switch (stat) {
                case 'int':
                    type = 'interceptions';
                    break;
                case 'pass_pd':
                    type = 'pass deflection';
                    break;
                case 'pass_td':
                    type = 'pass touchdowns';
                    break;
                case 'pass_yards':
                    type = 'pass yards';
                    break;
                case 'iay':
                    type = 'in air yards';
                    break;
                case 'rec_catch':
                    type = 'receptions';
                    break;
                case 'rec_yards':
                    type = 'receiving yards';
                    break;
                case 'rec_td':
                    type = 'receiving Touchdowns';
                    break;
                case 'sacks':
                    type = 'sacks';
                    break;
                case 'one':
                    type = 'frees';
                    break;
                case 'two':
                    type = 'Field goals';
                    break;
                case 'three':
                    type = 'Threes';
                    break;
                case 'ast':
                    type = 'assist';
                    break;
                case 'dbr':
                    type = 'D Rebounds';
                    break;
                case 'obr':
                    type = 'O Rebounds';
                    break;
                case 'stl':
                    type = 'steals';
                    break;
                case 'blk':
                    type = 'blocks';
                    break;
                case 'points':
                    type = 'points';
                    break;
                case 'pass_rate':
                    type = 'Rate';
                    break;
                case 'pass_comp':
                    type = 'Comps';
                    break;
                case 'pass_att':
                    type = 'Atts';
                    break;
                case 'pass_percentage':
                    type = 'pass %';
                    break;
                case 'pass_int':
                    type = 'pass int';
                    break;
                case 'pass_int_open':
                    type = 'pass int open';
                    break;
                case 'pass_int_xp':
                    type = 'pass int xp';
                    break;


                case 'pass_xp_1':
                    type = 'pass xp1';
                    break;
                case 'pass_xp_2':
                    type = 'pass xp2';
                    break;
                case 'pass_xp_3':
                    type = 'pass xp3';
                    break;

                case 'pd_percent':
                    type = 'QB Deflected %';
                    break;
                case 'pass_20_plus':
                    type = 'Pass 20+ Yards';
                    break;
                case 'pass_40_plus':
                    type = 'Pass 40+ Yards';
                    break;
                case 'yard_att':
                    type = 'Yards/Att';
                    break;
                case 'int_att':
                    type = 'Int/Att';
                    break;
                case 'td_att':
                    type = 'TD%';
                    break;
                case 'pass_4th_con':
                    type = '4th Con';
                    break;
                case 'qb_pd':
                    type = 'QB Pass Deflected';
                    break;
                case 'qb_drops':
                    type = 'QB Drops';
                    break;
                case 'drop_percent':
                    type = 'QB Drops  %';
                    break;
                case 'bt':
                    type = 'Bad Throws';
                    break;
                case 'sacked':
                    type = 'Sacked';
                    break;

                case 'rec_drop':
                    type = 'Drops';
                    break;
                case 'rec_xp_1':
                    type = 'Rec XP1';
                    break;
                case 'rec_xp_2':
                    type = 'Rec XP2';
                    break;
                case 'rec_xp_3':
                    type = 'Rec XP3';
                    break;
                case 'rec_10_plus':
                    type = 'Rec 10+ yards';
                    break;
                case 'rec_20_plus':
                    type = 'Rec 20+ yards';
                    break;
                case 'rec_40_plus':
                    type = 'Rec 40+ yards';
                    break;
                case 'yard_rec':
                    type = 'Yard/Rec';
                    break;
                case 'td_rec':
                    type = 'TD/Rec';
                    break;
                case 'drop_rec':
                    type = 'Drops/Rec';
                    break;
                case 'rec_4th_con':
                    type = '4th Con';
                    break;
                case 'first_downs_rec':
                    type = 'First Downs';
                    break;
                case 'yac':
                    type = 'YAC';
                    break;

                case 'run':
                    type = 'runs';
                    break;
                case 'run_yards':
                    type = 'rush yards';
                    break;
                case 'run_10_plus':
                    type = 'runs 10+';
                    break;
                case 'run_20_plus':
                    type = 'runs 20+';
                    break;
                case 'run_40_plus':
                    type = 'runs 40+';
                    break;
                case 'yard_run':
                    type = 'yard/run';
                    break;
                case 'run_td':
                    type = 'rush touchdown';
                    break;
                case 'run_xp_1':
                    type = 'Rush XP1';
                    break;
                case 'run_xp_2':
                    type = 'Rush XP2';
                    break;
                case 'run_xp_3':
                    type = 'Rush XP3';
                    break;
                case 'int_yards':
                    type = 'int yards';
                    break;
                case 'int_open':
                    type = 'int open';
                    break;
                case 'int_xp':
                    type = 'int xp';
                    break;
                case 'int_td':
                    type = 'defensive td';
                    break;
                case 'int_xp_1':
                    type = 'INT XP1';
                    break;
                case 'int_xp_2':
                    type = 'INT XP2';
                    break;
                case 'sack':
                    type = 'sack';
                    break;
                case 'safety':
                    type = 'safety';
                    break;
                case 'punt_td':
                    type = 'Punt touchdown';
                    break;
                case 'punt_yards':
                    type = 'Punt yards';
                    break;

                case 'kr_yards':
                    type = 'KR yards';
                    break;
                case 'pass_drop':
                    type = 'drops';
                    break;
                case 'flag_pull':
                    type = 'Flag Pull';
                    break;
                case 'psr':
                    type = 'RAC Rating';
                    break;
                default:
                    // code block
                    type = stat;
            }

            return type;
        },
        description_abb (stat) {
            let type;
            switch (stat) {
                case "pass_rate":
                    type = "Rating";
                    break;
                case "pass_att":
                    type = "Pass Att";
                    break;
                case "pass_comp":
                    type = "pass Comp";
                    break;
                case "pass_td":
                    type = "pass tds";
                    break;
                case "pass_percentage":
                    type = "pass %";
                    break;
                case "pass_xp_1":
                    type = "pass xp1";
                    break;
                case "pass_xp_2":
                    type = "pass xp2";
                    break;
                case "pass_xp_3":
                    type = "pass xp3";
                    break;
                case "pass_yards":
                    type = "pass Yards";
                    break;
                case "pass_20_plus":
                    type = "Passes 20+";
                    break;
                case "pass_40_plus":
                    type = "Passes 40+";
                    break;
                case "pass_int":
                    type = "Passes int";
                    break;
                case "yard_att":
                    type = "Yards Per Att";
                    break;
                case "td_att":
                    type = "tds Per Att";
                    break;
                case "int_att":
                    type = "Ints Per Att";
                    break;
                case "pass_4th_con":
                    type = "4th Down Con";
                    break;
                case "sacked":
                    type = "sacked";
                    break;
                case "qb_pd":
                    type = "QB PD";
                    break;
                case "pd_percent":
                    type = "QB PD %";
                    break;
                case "qb_drops":
                    type = "QB drops";
                    break;
                case "drop_percent":
                    type = "QB drops %";
                    break;
                case "bt":
                    type = "Bad Throws";
                    break;
                case "iay":
                    type = "In Air Yards";
                    break;
                case "pass_int_open":
                    type = "int Open";
                    break;
                case "pass_int_xp":
                    type = "int Xp";
                    break;
                case "run":
                    type = "Rush Atts";
                    break;
                case "run_yards":
                    type = "Rush Yards";
                    break;
                case "run_td":
                    type = "Rush tds";
                    break;
                case "run_xp_1":
                    type = "Rush xp1";
                    break;
                case "run_xp_2":
                    type = "Rush xp2";
                    break;
                case "run_xp_3":
                    type = "Rush xp3";
                    break;
                case "run_10_plus":
                    type = "Rush 10+";
                    break;
                case "run_20_plus":
                    type = "Rush 20+";
                    break;
                case "run_40_plus":
                    type = "Rush 40+";
                    break;
                case "yard_run":
                    type = "Yards/Run";
                    break;
                case "rec_td":
                    type = "Rec tds";
                    break;
                case "rec_yards":
                    type = "Rec Yards";
                    break;
                case "rec_catch":
                    type = "Rec";
                    break;
                case "rec_xp_1":
                    type = "Rec Xps 1";
                    break;
                case "rec_xp_2":
                    type = "Rec Xps 2";
                    break;
                case "rec_xp_3":
                    type = "Rec Xps 3";
                    break;
                case "rec_10_plus":
                    type = "Rec Yards 10+";
                    break;
                case "rec_20_plus":
                    type = "Rec Yards 20+";
                    break;
                case "rec_40_plus":
                    type = "Rec Yards 40+";
                    break;
                case "yard_rec":
                    type = "Yards per Rec";
                    break;
                case "td_rec":
                    type = "tds per Rec";
                    break;
                case "drop_rec":
                    type = "Drops/Rec";
                    break;
                case "rec_4th_con":
                    type = "Rec 4th Con";
                    break;
                case "first_downs_rec":
                    type = "Rec 1st Con";
                    break;
                case "yac":
                    type = "YAC";
                    break;
                case "pass_drop":
                    type = "Drops";
                    break;
                case "pass_pd":
                    type = "PD";
                    break;
                case "int":
                    type = "Ints";
                    break;
                case "int_open":
                    type = "Ints Open";
                    break;
                case "int_xp":
                    type = "Ints Xp";
                    break;
                case "int_yards":
                    type = "Ints yards";
                    break;
                case "int_xp_2":
                    type = "Xp return";
                    break;
                case "int_td":
                    type = "Ints TD";
                    break;
                case "flag_pull":
                    type = "Flag Pull";
                    break;
                case "punt_td":
                    type = "Punt tds";
                    break;
                case "punt_yards":
                    type = "Punt Yards";
                    break;
                case "kr_td":
                    type = "Kick tds";
                    break;
                case "kr_yards":
                    type = "Kick Yards";
                    break;

                default:
                    type = stat;
            }
            return type;
        },

        all_stats() {
            return [
                'pass_rate',
                'pass_att',
                'pass_comp',
                'pass_td',
                'pass_percentage',
                'pass_xp_1',
                'pass_xp_2',
                'pass_xp_3',
                'pass_yards',
                'pass_20_plus',
                'pass_40_plus',
                'pass_int',
                'yard_att',
                'td_att',
                'int_att',
                'pass_4th_con',
                'sacked',
                'qb_pd',
                'pd_percent',
                'drop_percent',
                'qb_drops',
                'bt',
                'iay',

                'pass_int_open',
                'pass_int_xp',


                'run',
                'run_yards',
                'run_td',
                'run_xp_1',
                'run_xp_2',
                'run_xp_3',
                'run_10_plus',
                'run_20_plus',
                'run_40_plus',
                'yard_run',


                'rec_td',
                'rec_yards',
                'rec_catch',
                'rec_xp_1',
                'rec_xp_2',
                'rec_xp_3',
                'rec_10_plus',
                'rec_20_plus',
                'rec_40_plus',
                'yard_rec',
                'td_rec',
                'drop_rec',
                'rec_4th_con',
                'first_downs_rec',
                'yac',
                'pass_drop',


                'pass_pd',
                'int',
                'int_open',
                'int_xp',
                'int_yards',
                'int_xp_2',
                'int_td',
                'sack',
                'safety',
                'flag_pull',

                'punt_td',
                'punt_yards',
                'kr_td',
                'kr_yards',
            ];
        },
        passing_stats() {
            return [
                'pass_rate',
                'pass_att',
                'pass_comp',
                'pass_td',
                'pass_percentage',
                'pass_xp_1',
                'pass_xp_2',
                'pass_xp_3',
                'pass_yards',
                'pass_20_plus',
                'pass_40_plus',
                'pass_int',
                'pass_int_open',
                'pass_int_xp',
                'yard_att',
                'td_att',
                'int_att',
                'pass_4th_con',
                'sacked',
                'qb_pd',
                'pd_percent',
                'drop_percent',
                'qb_drops',
                'bt',
                'iay',

            ]
        },
        rushing_stats() {

            return [
                'run',
                'run_yards',
                'run_td',
                'run_xp_1',
                'run_xp_2',
                'run_xp_3',
                'run_10_plus',
                'run_20_plus',
                'run_40_plus',
                'yard_run',
            ]
        },
        receiving_stats() {

            return [
                'rec_td',
                'rec_yards',
                'rec_catch',
                'rec_xp_1',
                'rec_xp_2',
                'rec_xp_3',
                'rec_10_plus',
                'rec_20_plus',
                'rec_40_plus',
                'yard_rec',
                'td_rec',
                'drop_rec',
                'rec_4th_con',
                'first_downs_rec',
                'yac',
                'pass_drop',
            ]
        },
        defense_stats() {

            return [
                'pass_pd',
                'int',
                'int_open',
                'int_xp',
                'int_yards',
                'int_xp_2',
                'int_td',
                'sack',
                'safety',
                'flag_pull',
            ]
        },
        operators_options() {
            return [
                '=',
                '<',
                '<=',
                '>',
                '>=',
                '!=',
            ]
        },
        omit() {
            return [
                'created_at',
                'id',
                'updated_at',
                'game_id',
                'user_id',
                'team_id',
                'game',
                'deleted_at'
            ]

        },
        passing_stats_main() {
            return [
                'pass_rate',
                'pass_percentage',
                'pass_yards',
                'pass_td',
            ]
        },
        rushing_stats_main() {

            return [
                'run',
                'run_yards',
                'run_td',
                'yard_run',
            ]
        },
        receiving_stats_main() {

            return [
                'rec_td',
                'rec_yards',
                'rec_catch',
                'yard_rec',
            ]
        },
        defense_stats_main() {

            return [
                'pass_pd',
                'int',
                'sack',
                'flag_pull',
            ]
        },


    }
}