export default {
  methods: {
      positionNames(string) {
          let title = 'Player';
          switch (string) {
              case 'qb':
                  title = 'quarterback';
                  break;
              case 'wr':
                  title = 'wide receiver';
                  break;
              case 'd':
                  title = 'defender';
                  break;
              case 'r':
                  title = 'rusher';
                  break;
              case 'c':
                  title = 'center';
                  break;
              case 'ol':
                  title = 'Oline';
                  break;
              case 'te':
                  title = 'tightend';
                  break;
              case 'lt':
                  title = 'left tackle';
                  break;
              case 'rt':
                  title = 'right tackle';
                  break;
              case 'rg':
                  title = 'right guard';
                  break;
              case 'lg':
                  title = 'left guard';
                  break;
              case 'rb':
                  title = 'Running Back';
                  break;
              case 'k':
                  title = 'Kicker';
                  break;
              case 'dl':
                  title = 'Dline';
                  break;
              case 'lb':
                  title = 'linebacker';
                  break;
              case 'co':
                  title = 'Coach';
                  break;
              case 'fs':
                  title = 'free safety ';
                  break;
              case 'ss':
                  title = 'strong safety';
                  break;
              case 'cb':
                  title = 'cornerback';
                  break;
          }
          return title
      },
      positionSelectValues() {

          return {
              'co': 'coach',
              'qb': 'quarterback',
              'wr': 'wide receiver',
              'd': 'defender',
              'r': 'rusher',
              'c': 'center',
              'ol': 'Oline',
              'te': 'tightend',
              'lt': 'left tackle',
              'rt': 'right tackle',
              'rg': 'right guard',
              'lg': 'left guard',
              'rb': 'Running Back',
              'k': 'kicker',
              'dl': 'Dline',
              'lb': 'linebacker',
              'fs': 'free safety ',
              'ss': 'strong safety',
              'cb': 'cornerback'
          }
      },
  }
}