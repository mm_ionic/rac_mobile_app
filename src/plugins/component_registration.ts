import * as components from '@ionic/vue';

class component_registration {
    private app: any;
    constructor(app: any) {
        this.app = app
    }

    init () {
        for (const [key, value] of Object.entries(components)) {
            this.app.component(key, value)
        }
    }
}

export default component_registration;