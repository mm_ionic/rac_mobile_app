import { Storage } from '@ionic/storage'

class myStorage {
    storage: any;
    constructor() {
        this.storage = new Storage()
        this.storage.create()
    }
}

export default new myStorage();

const myStorageConst = () => {
    return new myStorage()
}

import { App, Plugin } from 'vue';
// The Install function used by Vue to register the plugin
export const MyStoragePLugin: Plugin = {
    install(app: App, options: myStorage) {
        // makes ColoredText available in your Vue.js app as either "$this.coloredText" (in your Source) or "{{ $coloredText }}" in your templates
        app.config.globalProperties.$MyStorage = myStorageConst()
        // register Headline as a global component, so you can use it wherever you want in your app
        // app.component('Headline', Headline)

        app.provide('storage', options)
    }
}
