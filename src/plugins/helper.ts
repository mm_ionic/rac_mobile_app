import moment from 'moment'
class HelperClass {

  
  getWebAssetAbsoluteURL(url: string) {
    return process.env.VUE_APP_RACWEB_BASE_URL + url
  }

  clone (obj: any) {
    return JSON.parse(JSON.stringify(obj))
  }

  data_get<T>(obj: any, key: string, defaultVal?: T): T | undefined {
    const value = key.split('.').reduce((acc, currentKey) => {
      return acc ? acc[currentKey] : undefined;
    }, obj);
  
    return value !== undefined ? value : defaultVal;
  }


  formatDate (date: string, format: string) {

    const date1 = new Date(date)

    switch (format) {
      case 'date': {
        format = 'MMM Do, YYYY'
        break;
      }
      case 'time': {
        format = 'h:mm a'
        break;
      }
      case 'short_date': {
        format = 'MMM Do'
        break;
      }
    }
            
    return moment(date1).format(format)

  }
}


const helperCollection = () => {
    return new HelperClass()
}

import { App, Plugin } from 'vue';
// The Install function used by Vue to register the plugin
export const HelperPlugin: Plugin = {
    // install(app: App, options: HelperClass) {
    install(app: App, options: {store: any}) {
        app.config.globalProperties.$Helper = helperCollection()

        app.provide('Helper', options)
    }
}
