
export default {
  namespaced: true,
  state: {
    team: {},
    league: {},
    game: {},
  },
  mutations: {
    SET_TEAM (state: any, payload: any) {
      state.team = payload
    },
    SET_LEAGUE (state: any, payload: any) {
      state.league = payload
    },
    SET_GAME (state: any, payload: any) {
      state.game = payload
    }
  },
  actions: {
    SET_TEAM (context: any, payload: any) {
      context.commit('SET_TEAM', payload)
    },
    SET_LEAGUE (context: any, payload: any) {
      context.commit('SET_LEAGUE', payload)
    },
    SET_GAME (context: any, payload: any) {
      context.commit('SET_GAME', payload)
    }
  },
  getters: {
    GET_TEAM (state: any) {
      return state.team
    },
    GET_LEAGUE (state: any) {
      return state.league
    },
    GET_GAME (state: any) {
      return state.game
    }
  }
};
