import {loadingController, alertController} from '@ionic/vue';

export default {
  namespaced: true,
  state: {
    loading: {},
    alert: {},
    game: {},
  },
  mutations: {
    SET_LOADING (state: any, payload: any) {
      state.loading = payload
    },
    SET_ALERT (state: any, payload: any) {
      state.alert = payload
    },
  },
  actions: {
    async SHOW_LOADING (context: any, payload: any) {
      const loading = await loadingController.create({
        message: payload,
        duration: 5000
      });
  
      context.commit('SET_LOADING', loading)
      context.state.loading.present()
    },
    async HIDE_LOADING (context: any) {
      context.state.loading.dismiss()
    },
    async SHOW_ALERT (context: any, payload: any) {
      const alert = await alertController.create({
        header: payload.header,
        message: payload.message,
        buttons: ['OK']
      });
      await alert.present();
  
      if (payload.autohide) {
        setTimeout(()=>alert.dismiss(), payload.timer);
      }

      context.commit('SET_ALERT', alert)
    }
  },
  getters: {
    GET_LOADING (state: any) {
      return state.loading
    },
    GET_ALERT (state: any) {
      return state.alert
    }
  }
};
