
import myStorage from '../../plugins/ionStorage'
import api from '../../api/api'

import { useRouter } from 'vue-router'

const router = useRouter()

export default {
  namespaced: true,
  state: {
    user: {},
    token: myStorage.storage.get('token')
  },
  mutations: {
    SET_USER (state: any, payload: any) {
      state.user = payload
    },
    SET_TOKEN (state: any, payload: any) {
      state.token = payload
    },
  },
  actions: {
    async SET_TOKEN(context: any, payload: any) {
      await myStorage.storage.set('token', payload)
      context.commit('SET_TOKEN', payload);
    },
    async LOGOUT (context: any) {
      await myStorage.storage.clear()
      context.commit('SET_TOKEN', '');
      router.push('/')
    },
    async ME (context: any) {
      const response = await api.user.me()
      context.commit('SET_USER', response.data)
    }
  },
  getters: {
    GET_USER (state: any) {
      return state.user
    },

    GET_TOKEN (state: any) {
      return state.token
    }
  }
};
