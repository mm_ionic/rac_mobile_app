import { createStore } from 'vuex'
import user from './modules/user'
import app from './modules/app' 
import ui from './modules/ui' 

export const store = createStore({
  modules: {
    user: user,
    app: app,
    ui: ui
  }
});
