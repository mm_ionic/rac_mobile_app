import {
    createStore,
    MutationTree,
    ActionContext,
    ActionTree,
    GetterTree,
    Store as VuexStore,
    CommitOptions,
    DispatchOptions,
    createLogger
  } from "vuex";

import { useRouter } from 'vue-router';
import { Storage } from '@ionic/storage';

import myStorage from '../plugins/ionStorage'

const storage = new Storage();

const router = useRouter()
  
  //declare state
  export type State = { 
    counter: number,
    user: object ,
    team: object,
    league: object,
    game: object,
    token: string | null
  };

  
  //set state
  const state: State = { 
    counter: 0,
    user: {},
    team: {},
    league: {},
    game: {},
    token: myStorage.storage.get('token')
  };
  
  // mutations and action enums
  
  export enum MutationTypes {
    INC_COUNTER = "SET_COUNTER",
    SET_USER = "SET_USER",
    SET_TOKEN = "SET_TOKEN",
    SET_TEAM = "SET_TEAM",
    SET_GAME = "SET_GAME",
    SET_LEAGUE = "SET_LEAGUE",
  }
  
  export enum ActionTypes {
    INC_COUNTER = "SET_COUNTER",
    SET_USER = "SET_USER",
    SET_TOKEN = "SET_TOKEN",
    SET_TEAM = "SET_TEAM",
    SET_GAME = "SET_GAME",
    SET_LEAGUE = "SET_LEAGUE", 
    LOGOUT = "LOGOUT"
  }
  
  //Mutation Types
  export type Mutations<S = State> = {
    [MutationTypes.INC_COUNTER](state: S, payload: number): void;
    [MutationTypes.SET_USER](state: S, payload: object): void;
    [MutationTypes.SET_TOKEN](state: S, payload: string): void;
    [MutationTypes.SET_TEAM](state: S, payload: object): void;
    [MutationTypes.SET_GAME](state: S, payload: object): void;
    [MutationTypes.SET_LEAGUE](state: S, payload: object): void;
  };
  
  //define mutations
  const mutations: MutationTree<State> & Mutations = {
    [MutationTypes.INC_COUNTER](state: State, payload: number) {
      state.counter += payload;
    },

    [MutationTypes.SET_USER](state: State, payload: object) {
      state.user = payload;
    },

    [MutationTypes.SET_TOKEN](state: State, payload: string) {
      state.token = payload;
    },

    [MutationTypes.SET_TEAM](state: State, payload: object) {
      state.team = payload;
    },

    [MutationTypes.SET_GAME](state: State, payload: object) {
      state.game = payload;
    },

    [MutationTypes.SET_LEAGUE](state: State, payload: object) {
      state.league = payload;
    },
  };
  
  //actions
  
  type AugmentedActionContext = {
    commit<K extends keyof Mutations>(
      key: K,
      payload: Parameters<Mutations[K]>[1]
    ): ReturnType<Mutations[K]>;
  } & Omit<ActionContext<State, State>, "commit">;
  
  // actions interface
  
  export interface Actions {
    [ActionTypes.INC_COUNTER](
      { commit }: AugmentedActionContext,
      payload: number
    ): void;
    [ActionTypes.SET_TOKEN](
      { commit }: AugmentedActionContext,
      payload: string
    ): void;
    [ActionTypes.SET_TEAM](
      { commit }: AugmentedActionContext,
      payload: object
    ): void;
    [ActionTypes.SET_LEAGUE](
      { commit }: AugmentedActionContext,
      payload: object
    ): void;

    [ActionTypes.SET_GAME](
      { commit }: AugmentedActionContext,
      payload: object
    ): void;
    
    [ActionTypes.LOGOUT](
      { commit }: AugmentedActionContext,
    ): void;
  }
  
  export const actions: ActionTree<State, State> & Actions = {
    [ActionTypes.INC_COUNTER]({ commit }, payload: number) {
      commit(MutationTypes.INC_COUNTER, payload);
    },
    async [ActionTypes.SET_TOKEN]({ commit }, payload: string){
      await myStorage.storage.set('token', payload)
      commit(MutationTypes.SET_TOKEN, payload);
    },

    [ActionTypes.SET_TEAM]({ commit }, payload: object){
      commit(MutationTypes.SET_TEAM, payload);
    },

    [ActionTypes.SET_GAME]({ commit }, payload: object){
      commit(MutationTypes.SET_GAME, payload);
    },

    [ActionTypes.SET_LEAGUE]({ commit }, payload: object){
      commit(MutationTypes.SET_LEAGUE, payload);
    },

    async [ActionTypes.LOGOUT]({ commit }) {
      await myStorage.storage.clear()
      commit(MutationTypes.SET_TOKEN, '');
      router.push('/')
    },

  };
  
  // Getters types
  export type Getters = {
    doubleCounter(state: State): number;
    userToken(state: State): string | null;
    teamDetail(state: State): object | null;
    gameDetail(state: State): object | null;
    leagueDetail(state: State): object | null;
    user(state: State): object | null;
  };
  
  //getters
  
  export const getters: GetterTree<State, State> & Getters = {
    doubleCounter: state => {
      console.log("state", state.counter);
      return state.counter * 2;
    },
    teamDetail: state => {
      return state.team
    },
    gameDetail: state => {
      return state.game
    },
    leagueDetail: state => {
      return state.league
    },
    userToken: state => {
      return state.token;
    },
    user: state => {
      return state.user
    }
  };
  
  //setup store type
  export type Store = Omit<
    VuexStore<State>,
    "commit" | "getters" | "dispatch"
  > & {
    commit<K extends keyof Mutations, P extends Parameters<Mutations[K]>[1]>(
      key: K,
      payload: P,
      options?: CommitOptions
    ): ReturnType<Mutations[K]>;
  } & {
    getters: {
      [K in keyof Getters]: ReturnType<Getters[K]>;
    };
  } & {
    dispatch<K extends keyof Actions>(
      key: K,
      payload: Parameters<Actions[K]>[1],
      options?: DispatchOptions
    ): ReturnType<Actions[K]>;
  };
  
  export const store = createStore({
    state,
    mutations,
    actions,
    getters,
    plugins: [createLogger()]
  });
  
  export function useStore() {
    return store as Store;
  }
  // export const store = createStore({
  //   state,
  //   mutations: {
  //     increment(state, payload) {
  //       state.counter++;
  //     }
  //   },
  //   actions: {
  //     increment({ commit }) {
  //       commit("increment");
  //     }
  //   },
  
  //   getters: {
  //     counter(state) {
  //       return state.counter;
  //     }
  //   },
  //   modules: {}
  // });
  