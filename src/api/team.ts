import http from "../http-common";

class TeamApi {
  my_team(user_id: any): Promise<any> {
    return http.post("/myTeams?user_id=" + user_id);
  }
  team_games(params: any): Promise<any> {
    return http.post("/teamGames", null, { 
        params: params
    });
  }

  roster(team_id: any): Promise<any> {
    return http.post("/roster?team_id="+team_id);
  }

  fetchStats (params: any): Promise<any> {
    
    const esc = encodeURIComponent;

    const query = Object.keys(params)
      .map(k => esc(k) + '=' + esc(params[k]))
      .join('&');

    return http.post("/fetchStats?" + query);

  }
  teamGame (params: any): Promise<any> {
    
    const esc = encodeURIComponent;

    const query = Object.keys(params)
      .map(k => esc(k) + '=' + esc(params[k]))
      .join('&');

    return http.post("/teamGames?" + query);

  }
}
export default new TeamApi();