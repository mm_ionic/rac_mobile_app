import http from "../http-common";

class LeagueApi {
    index(params: any): Promise<any> {
        return http.post("/leagues", null, { 
            params: params
        });
    }
    leagues_game(params: any): Promise<any> {
        return http.post("/leagueGames", null, { 
            params: params
        });
    }
    
}
export default new LeagueApi();