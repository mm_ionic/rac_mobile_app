import http from "../http-common";
class UserAPI {
  me(): Promise<any> {
    return http.get("/me");
  }
  login(data: any): Promise<any>{
    return http.post("/login", data);
  }
  player (params: any): Promise<any> {
    
    const esc = encodeURIComponent;

    const query = Object.keys(params)
      .map(k => esc(k) + '=' + esc(params[k]))
      .join('&');

    return http.post("/player?" + query);
  }
  update (data: any): Promise<any> {
    return http.post("/playerUpdate", data)
  }
}
export default new UserAPI();