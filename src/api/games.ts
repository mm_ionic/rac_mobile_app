import http from "../http-common";

class GamesApi {
   index(params: any): Promise<any> {
    return http.post("/leagues", null, { 
        params: params
    });
  }
  my_upcoming_games(params: any): Promise<any> {
    return http.post("/myUpcomingGames", null, { 
        params: params
    });
  }
  my_recent_games(params: any): Promise<any> {
    return http.post("/myRecentGames", null, { 
        params: params
    });
  }
  
  detail(game_id: any): Promise<any> {
    return http.post("/game", null, { 
        params: {
          game_id: game_id
        }
    });
  }

}
export default new GamesApi();