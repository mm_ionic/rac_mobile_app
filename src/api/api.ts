import UserAPI from "./user"
import TeamAPI from "./team"
import LeagueAPI from "./league"
import GamesAPI from "./games"

class api {
    user: typeof UserAPI;
    team: typeof TeamAPI;
    games: typeof GamesAPI;
    league: typeof LeagueAPI;

    constructor() {
        this.user = UserAPI;
        this.team = TeamAPI;
        this.games = GamesAPI;
        this.league = LeagueAPI;
    }
}

export default new api();

const apiCollection = () => {
    return new api()
}

import { App, Plugin } from 'vue';
// The Install function used by Vue to register the plugin
export const ApiPlugin: Plugin = {
    install(app: App, options: api) {
        // makes ColoredText available in your Vue.js app as either "$this.coloredText" (in your Source) or "{{ $coloredText }}" in your templates
        app.config.globalProperties.$ApiPlugin = apiCollection()
        // register Headline as a global component, so you can use it wherever you want in your app
        // app.component('Headline', Headline)

        app.provide('ApiPlugin', options)
    }
}
