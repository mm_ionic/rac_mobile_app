import axios, { AxiosInstance } from "axios";
// import { useStore, MutationTypes, ActionTypes } from "./store";

import {store} from "./store"


const apiClient: AxiosInstance = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Accept': 'application/json, text/plain'
  },
});

apiClient.interceptors.request.use(async function(config: any) {
  const token = await store.getters['user/GET_TOKEN'];
  // console.log(store.getters, 'token')
    if(token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
}, function(err) {
    return Promise.reject(err);
});

export default apiClient;