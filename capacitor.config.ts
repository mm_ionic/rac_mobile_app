import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'org.racsports.app',
  appName: 'Rac Sports',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
